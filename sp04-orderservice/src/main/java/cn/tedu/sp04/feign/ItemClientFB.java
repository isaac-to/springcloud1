package cn.tedu.sp04.feign;

import cn.tedu.sp01.pojo.Item;
import cn.tedu.web.util.JsonResult;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ItemClientFB implements ItemClient{
    @Override
    public JsonResult<List<Item>> getItems(String orderId) {
        //模拟返回缓存数据
        if (Math.random() < 0.5){ //如果有缓存,返回缓存数据
            List<Item> list = new ArrayList<>();
            list.add(new Item(1,"缓存商品1",2));
            list.add(new Item(2,"缓存商品2",1));
            list.add(new Item(3,"缓存商品3",3));
            list.add(new Item(4,"缓存商品4",5));
            list.add(new Item(5,"缓存商品5",4));
            return JsonResult.ok().data(list);
        }
        return JsonResult.err("获取商品订单失败");
    }

    @Override
    public JsonResult<?> decreaseNumber(List<Item> items) {
        return JsonResult.err("修改商品库存失败");
    }
}
