package cn.tedu.sp02.item.service;

import cn.tedu.sp01.pojo.Item;
import cn.tedu.sp01.service.ItemService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class ItemServiceImpl implements ItemService {

    @Override
    public List<Item> getItems(String orderId) {
         /*
        demo 演示代码，没有数据库，
        数据在这里写死
         */
        List<Item> list = new ArrayList<Item>();
        list.add(new Item(1,"商品 1",1));
        list.add(new Item(2,"商品 2",4));
        list.add(new Item(3,"商品 3",2));
        list.add(new Item(4,"商品 4",6));
        list.add(new Item(5,"商品 5",1));
        return list;
    }

    @Override
    public void decreaseNumber(List<Item> list) {
        // items.for 快捷提示
        for(Item item : list){
            log.info("减少库存 - "+item);
        }
    }
}
