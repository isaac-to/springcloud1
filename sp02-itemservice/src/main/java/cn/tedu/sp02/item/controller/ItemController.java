package cn.tedu.sp02.item.controller;

import cn.tedu.sp01.pojo.Item;
import cn.tedu.sp01.service.ItemService;
import cn.tedu.web.util.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Random;

@Slf4j
@RestController
public class ItemController {
    @Autowired
    private ItemService itemService;
    @Value("${server.port}")
    private Integer port;

    //根据订单id获取商品列表
    @GetMapping("/{orderId}")
    public JsonResult<List<Item>> getItems(@PathVariable String orderId) throws InterruptedException {
        log.info("获取商品列表：orderId="+orderId);

        //模拟超时失败
        if(Math.random() < 0.9){ // 90%概率执行延迟
            //随机延迟时长 0 - 5 秒
            long t = new Random().nextInt(5000);
            System.out.println("延迟: "+t);
            Thread.sleep(t);


        }

        List<Item> items = itemService.getItems(orderId);
        return JsonResult.ok().data(items).msg("port="+port);
    }


    /*
    @RequestBody 完整接收请求协议体的数据
     */
    //减少商品库存
    @PostMapping("/decreaseNumber")
    public JsonResult<?> decreaseNumber(@RequestBody List<Item> items) {
        itemService.decreaseNumber(items);
        return JsonResult.ok().msg("减少商品库存成功");
    }
}
