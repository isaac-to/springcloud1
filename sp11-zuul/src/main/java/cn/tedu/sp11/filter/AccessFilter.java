package cn.tedu.sp11.filter;

import cn.tedu.web.util.JsonResult;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.apache.commons.lang.StringUtils;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;

import javax.servlet.http.HttpServletRequest;

public class AccessFilter extends ZuulFilter {
    // 指定过滤器的类型， "pre", "routes", "post", "error"
    @Override
    public String filterType() {
        return null;
    }

    // 指定过滤器添加的位置序号
    @Override
    public int filterOrder() {
        return 6;
    }

    // 判断针对当前这次请求，是否要执行过滤代码
    // 如果请求 item-service，执行全选判断
    // 如果请求 user 或 order，直接跳过过滤代码
    @Override
    public boolean shouldFilter() {
        RequestContext ctx = RequestContext.getCurrentContext();
        String serviceId = (String) ctx.get(FilterConstants.SERVICE_ID_KEY);

        return "item-service".equalsIgnoreCase(serviceId);
    }

    @Override
    public Object run() throws ZuulException {
        // http://localhost:3001/item-service/6y45634?token=245234243

        /*
        1.接收 token 参数
        2.如果没有 token，阻止继续调用，直接返回登录提示
         */

        //得到上下文对象，再从上下文得到 request 对象
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        String token = request.getParameter("token");
        if (StringUtils.isBlank(token)) {
            //阻止继续调用后台服务
            ctx.setSendZuulResponse(false);

            //直接向客户端返回响应
            ctx.setResponseStatusCode(401);
            ctx.addZuulResponseHeader("Content-Type", "application/json;charset=UTF-8");
            ctx.setResponseBody(JsonResult.err().code(401).msg("not login！ 未登录！").toString());
        }

        return null; //这个返回值目前没有任何作用，可能会在未来版本中发挥作用
    }
}
