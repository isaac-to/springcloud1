package cn.tedu.sp03.user.service;

import cn.tedu.sp01.pojo.User;
import cn.tedu.sp01.service.UserService;
import cn.tedu.web.util.JsonUtil;

import com.fasterxml.jackson.core.type.TypeReference;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class UserServiceImpl implements UserService {
    // 把yml中配置的属性值注入到这个变量
    // yml中如果没有这项属性，项目启动会失败
    @Value("${sp.user-service.users}")
    private String userJson;

    @Override
    public User getUser(Integer userId) {
        log.info("userId"+userId+", userJson="+userJson);

        // json转换：把 userJson 转换成 List<User>
        List<User> list = JsonUtil.from(userJson, new TypeReference<List<User>>() {});

        for (User u : list) {
            if (userId.equals(u.getId())) {
                return u; //找到用户直接返回
            }
        }

        //如果不是测试用的用户数据，返回一个写死的用户数据
        return new User(userId, "用户名"+userId, "密码"+userId);
    }
    @Override
    public void addScore(Integer userId, Integer score) {
        log.info("增加用户积分，userId="+userId+"， score="+score);
    }
}